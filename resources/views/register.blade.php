<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Sign Up || SanberBook</title>
</head>

<body>
<form action="/welcome" method="post">
<h1>Buat Account Baru! </h1>
<h3>Sign Up Form</h3>
@csrf
  First Name: <br /> <br />
  <input type="text"  name="firstName"/> <br /><br />
  Last Name: <br /><br />
  <input type="text" name="lastName"/><br /><br />
  Gender: <br /><br />
  <input type="radio" value="Male" name="gender"/> Male <br />
  <input type="radio" value="Female" name="gender"/> Female <br />
  <input type="radio" value="Other" name="gender"/> Other <br /><br />
  Nationality :
  <br />
  <select name="nationality">
  <option> Indonesia </option>
  <option> Non-Indonesia </option>
  </select>
  <br /><br />
  Language Spoken :<br /><br />
  <input type="checkbox" name="bahasa" value="Bahasa Indonesia" /> Bahasa Indonesia <br />
  <input type="checkbox" name="bahasa" value="English" /> English <br />
  <input type="checkbox" name="bahasa" value="Other" /> Other <br />
<br />
Bio <br />
<textarea name="bio" cols="50" rows="7"></textarea> <br /><br />
<input type="submit" value="Sign Up" name="signUp" /> 
    
</form>
</body>
</html>
